#include <thread>
#include <iostream>

#include "ScriptManager.h"
#include "Mouse.h"
#include "Buttons.h"

ScriptManager::ScriptManager(std::string type)
{
	state = false;
	scriptsDictionary = { {"Default", new ScriptStandart(type) }, {"Notepad", new ScriptNotepad()}, {"OpusApp", new ScriptWord()}, {"SALFRAME", new ScriptWord()}, {"AcrobatSDIWindow", new ScriptPDF()} };
	this->AddScript("Default", GetDesktopWindow());
	scripts.front()->SetState(true);
}

void ScriptManager::Start()
{
	srand(time(NULL));
	state = true;

	while (state)
	{
		std::cout << "Scripts size: " << scripts.size() << std::endl;

		if (!scripts.empty())
		{
			if (!scripts.front()->IsWindowOpen())
			{
				pids.erase(scripts.front()->GetPid());
				delete scripts.front();
				scripts.pop_front();
			}
			else
			{
				scripts.front()->Work();

				if (scripts.front()->IsDone())
				{
					pids.erase(scripts.front()->GetPid());
					delete scripts.front();
					scripts.pop_front();
				}
			}
		}
		else
		{
			//AddScript("Default", GetDesktopWindow());
			//scripts.front()->SetState(true);
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	}
}

Script* ScriptManager::AddScript(std::string name, HWND window, DWORD pid)
{
	if (scriptsDictionary.count(name))
	{
		std::cout << "New script " << name << std::endl;

		Script* script = scriptsDictionary[name]->Init(window);
		script->SetPid(pid);
		script->SetWindow(window);
		scripts.push_front(script);
		return script;
	}
	else
	{
		std::cout << "No script " << name << std::endl;
		return nullptr;
	}
}

Script* ScriptManager::AddScript(Script* script)
{
	scripts.push_front(script);
	return script;
}

void ScriptManager::ClearScriptsQueue()
{
	for (auto script : scripts)
	{
		delete script;
	}
	scripts.clear();
}

void ScriptManager::Notificate(Event* event)
{
	StopCurrentScript();

	switch (event->GetType())
	{
	case Events::NewForegraundWindow:
	{
		EventNewForegraundWindow* _event = reinterpret_cast<EventNewForegraundWindow*>(event);
		DWORD pid = _event->_pid;

		std::cout << "New event with pid: " << pid << std::endl;

		if (_event->_headerName == "Microsoft Office Activation Wizard")
		{
			AddScript(Mouse::GetMouse()->CloseWindow(_event->windowHandle));
			break;
		};

		if (pids.count(pid))
		{
			pids[pid]->SetState(true);
		}
		else
		{
			Script* script = AddScript(_event->_className, _event->windowHandle, pid);

			if (script)
			{
				std::pair<DWORD, Script*> tmp(pid, script);
				pids.insert(tmp);
			}
			else
			{

				ButtonParams params;

				WNDENUMPROC func = [](HWND window, LPARAM lParam) -> BOOL
				{
					ButtonParams* param = (ButtonParams*)(lParam);

					RECT rect;

					char name[MAX_PATH];

					SendMessageA(window, WM_GETTEXT, MAX_PATH, (LPARAM)name);

					std::string tmp = name;

					if (tmp.empty())
					{
						return TRUE;
					}

					std::cout << "TextWindow before: " << tmp << std::endl;

					char bad[] = { '&', ' ', '<', '>' };

					size_t pos;

					for (auto symbol : bad)
					{
						do
						{
							pos = tmp.find(symbol);

							if (pos != std::string::npos)
							{
								tmp.erase(pos, 1);
							}
						} while (pos != std::string::npos);
					}

					std::cout << "TextWindow after: " << tmp << std::endl;

					for (int i = 0; i < ButtonParams::agreements.size(); ++i)
					{
						if (!_strcmpi(ButtonParams::agreements[i].c_str(), tmp.c_str()))
						{
							std::cout << "Button agreements: " << tmp << std::endl;
							param->window = window;
							return FALSE;
						}
					}

					for (int i = 0; i < ButtonParams::buttons.size(); ++i)
					{
						if (!_strcmpi(ButtonParams::buttons[i].c_str(), tmp.c_str()))
						{
							std::cout << "Button: " << tmp << std::endl;
							param->window = window;
							return FALSE;
						}
					}

					for (int i = 0; i < ButtonParams::exclude.size(); ++i)
					{
						if (!_strcmpi(ButtonParams::exclude[i].c_str(), tmp.c_str()))
						{
							std::cout << "Button exclude: " << tmp << std::endl;
							param->window = window;
							return FALSE;
						}
					}

					return TRUE;

				};

				EnumChildWindows(_event->windowHandle, func, (LPARAM)&params);

				RECT rect;

				if (params.window)
				{
					GetWindowRect(params.window, &rect);

					std::cout << "Button x: " << rect.left << " y: " << rect.top << std::endl;

					script = new Script();

					script->AddAction(Mouse::GetMouse()->MoveCursorTo(rect.left + 3, rect.top + 3));
					script->AddAction(Mouse::GetMouse()->LeftButtonClick());

					script->SetState(true);
					script->SetPid(pid);
					script->SetWindow(_event->windowHandle);

					/*
					std::pair<DWORD, Script*> tmp(pid, script);
					pids.insert(tmp);
					*/
					AddScript(script);

				}
			}
		}
		break;
	}
	case Events::Exit:
	{
		state = false;
		delete event;
		ClearScriptsQueue();
		exit(0);
		break;
	}
	default:
		break;
	}

	if (!scripts.empty())
	{
		scripts.front()->SetState(true);
	}
	delete event;
}

void ScriptManager::StopCurrentScript()
{
	if (!scripts.empty())
	{
		scripts.front()->Stop();
	}
}
