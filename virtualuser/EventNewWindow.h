#pragma once
#include "Event.h"

class EventNewForegraundWindow : public Event
{
public:
	EventNewForegraundWindow(HWND wnd, char* className, char* headerName, DWORD pid) : Event(Events::NewForegraundWindow), windowHandle(wnd), _className(className), _headerName(headerName), _pid(pid) {}

	HWND windowHandle;
	char* _className;
	char* _headerName;
	DWORD _pid;
};

