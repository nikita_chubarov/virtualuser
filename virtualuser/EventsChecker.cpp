#include "EventsChecker.h"
#include <iostream>


EventsChecker::EventsChecker()
{
	desktopHandler = GetDesktopWindow();
	currentForegraundWindow = GetForegroundWindow();
}

void EventsChecker::CheckForegraundWindow()
{
	srand(time(NULL));

	EventNewForegraundWindow* event;
	HWND windowHandle;
	DWORD threadProcId = -1;

	char windowClassName[MAX_PATH];

	while (true) 
	{
		windowHandle = GetForegroundWindow();

		if (currentForegraundWindow != windowHandle && (windowHandle != nullptr))
		{
			currentForegraundWindow = windowHandle;

			GetClassNameA(windowHandle, windowClassName, MAX_PATH);
			std::cout << "ClassName: " << windowClassName << std::endl;

			char windowHeaderName[MAX_PATH];
			GetWindowTextA(windowHandle, windowHeaderName, MAX_PATH);
			std::cout << "Header name: " << windowHeaderName << std::endl;

			
			if(ignoredWindowClasses.count(windowClassName) || ignoredWindowHeaders.count(windowHeaderName))
			{
				continue;
			}


			GetWindowThreadProcessId(windowHandle, &threadProcId);

			event = new EventNewForegraundWindow(windowHandle, windowClassName, windowHeaderName, threadProcId);

			NotificateAll(event);
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}

void EventsChecker::NotificateAll(Event* event)
{
	for (size_t i = 0; i < subscribers.size(); ++i)
	{
		subscribers[i]->Notificate(event);
	}
}

void EventsChecker::Subscribe(Subscriber* subscriber)
{
	subscribers.push_back(subscriber);
}

void EventsChecker::StartCheckEvents()
{
	std::thread checkWnd(&EventsChecker::CheckForegraundWindow, this);
	std::thread checkExit(&EventsChecker::CheckExit, this);

	checkExit.detach();
	checkWnd.detach();
}

void EventsChecker::CheckExit()
{
	while (true) 
	{
		if ((GetAsyncKeyState(VK_CONTROL) & 0x8000 && GetAsyncKeyState(VK_END) & 0x8000))
		{
			EventExit* event = new EventExit();
			NotificateAll(event);
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
}
