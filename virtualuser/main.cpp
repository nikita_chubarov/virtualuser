#include <iostream>
#include <Windows.h>
#include <sstream>
#include <fstream>
#include <time.h>

#include "EventsChecker.h"

using namespace std;

int AcquireDebugPrivileges()
{
	HANDLE hToken;
	TOKEN_PRIVILEGES tokenPriv;
	LUID luidDebug;

	if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken))
	{
		if (LookupPrivilegeValueW(L"", SE_DEBUG_NAME, &luidDebug))
		{
			tokenPriv.PrivilegeCount = 1;
			tokenPriv.Privileges[0].Luid = luidDebug;
			tokenPriv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

			if (AdjustTokenPrivileges(hToken, FALSE, &tokenPriv, sizeof(tokenPriv), NULL, NULL) == 0)
			{
				return 1;
				// error msg
			}
			return 0;
		}
		return 2;
	}
	return 3;
}

int EnableMouseCursor()
{
	HINSTANCE hinst;            // handle to current instance 
	HCURSOR hCurs1;
	hCurs1 = LoadCursor(NULL, IDC_WAIT);
	SetCursor(hCurs1);
	// OCR_APPSTARTING
	// 32650
	const int OCR_APPSTARTING = 32650;

	if (!SetSystemCursor(hCurs1, OCR_APPSTARTING))
	{
		std::cout << GetLastError() << std::endl;
	}
	ShowCursor(TRUE);

	return 0;
}


int main(int argc, char* argv[])
{
	std::cout << "Enable cursor status: " << EnableMouseCursor() << std::endl;

	setlocale(LC_ALL, "");
	if (int err = AcquireDebugPrivileges())
	{

		exit(err + 33);
	}
	
	std::string type = "notepad";

	if (argc > 1)
	{
		type = argv[1];
	}

	ScriptManager* scriptManager = new ScriptManager(type);
	EventsChecker* checker = new EventsChecker();

	checker->Subscribe(scriptManager);
	checker->StartCheckEvents();

	std::cout << "Default script imitation run!" << std::endl;

	std::thread startThread(&ScriptManager::Start, scriptManager);
	startThread.join();

	return 0;
}