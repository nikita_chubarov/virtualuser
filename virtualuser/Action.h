#pragma once

class Action
{
public:
	Action();
	virtual void Do() = 0;
};

