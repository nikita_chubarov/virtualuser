#include "Script.h"

Script::Script()
{
	_state = false;
	isDone = false;
}

void Script::AddAction(Action* action)
{
	actions.push_back(action);
}

void Script::AddAction(Script* script)
{
	for (std::deque<Action*>::iterator it = script->actions.begin(); it != script->actions.end(); ++it)
	{
		actions.push_back(*it);
	}
}

void Script::AddAction(std::deque<Action*>* script)
{
	for (std::deque<Action*>::iterator it = script->begin(); it != script->end(); ++it)
	{
		actions.push_back(*it);
	}
}


Action* Script::SleepAction(int milliseconds)
{
	ActionSleep* action = new ActionSleep(milliseconds);
	return action;
}

void Script::SetWindowRect(HWND _windowHandle)
{
	windowHandle = _windowHandle;
	GetWindowRect(_windowHandle, &windowRect);
}

RECT Script::GetCurrentWindowRect() const
{
	return windowRect;
}


void Script::SetWindow(HWND wnd)
{
	windowHandle = wnd;
}

HWND Script::GetCurrentWindow() const
{
	return windowHandle;
}

bool Script::IsWindowOpen()
{
	RECT r;
	return GetWindowRect(windowHandle, &r);
}

DWORD Script::GetPid() const
{
	return _pid;
}

void Script::SetPid(DWORD pid)
{
	_pid = pid;
}

void Script::ActionSleep::Do()
{
	Sleep(_milliseconds);
}

void Script::Work()
{
	while (_state && !isDone)
	{
		if (!actions.empty())
		{
			if (IsWindowOpen())
			{
				actions.front()->Do();
				delete actions.front();
				actions.pop_front();

				if (actions.empty())
				{
					isDone = true;
					_state = false;
					break;
				}
			}
			else
			{
				isDone = true;
				_state = false;
				break;

			}
		}
	}
}

void Script::Stop()
{
	if (!isDone)
	{
		this->_state = false;
	}
}

bool Script::IsDone() const
{
	return isDone;
}

void Script::SetState(bool state)
{
	_state = state;
}
