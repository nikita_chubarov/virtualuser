#pragma once
#include "Script.h"

class ScriptPDF : public Script
{
public:
	ScriptPDF();

	int GetPagesCount(HWND);

	Script* Init(HWND) override;
	
};

