#include "ScriptNotepad.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "Texts.h"

ScriptNotepad::ScriptNotepad()
{
}

Script* ScriptNotepad::Init(HWND window)
{
	Script* script = new Script();

	Keyboard* keyboard = Keyboard::GetKeyboard();
	Mouse* mouse = Mouse::GetMouse();
	
	HWND wnd = FindWindowExA(window, NULL, "Edit", NULL);
	
	script->SetWindowRect(wnd);
	RECT rect = script->GetCurrentWindowRect();

	script->AddAction(mouse->MoveCursorTo(rect.left + 20, rect.top + 20));
	script->AddAction(mouse->LeftButtonClick());

	script->AddAction(keyboard->PressEnd());

	int random = rand() % Texts::texts.size();

	script->AddAction(keyboard->PrintText(Texts::texts[random]));
	script->AddAction(keyboard->PressCtrlS());

	script->AddAction(mouse->CloseWindow(window));

	return script;
}
