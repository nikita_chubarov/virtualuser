#include "ScriptWord.h"
#include "Texts.h"
#include "Keyboard.h"
#include "Mouse.h"

ScriptWord::ScriptWord()
{
}

Script* ScriptWord::Init(HWND window)
{
	Script* script = new Script();

	Keyboard* keyboard = Keyboard::GetKeyboard();
	Mouse* mouse = Mouse::GetMouse();

	HWND wnd = FindWindowExA(window, NULL, "_WwG", NULL);

	script->SetWindowRect(wnd);

	LONG yfinish = script->GetCurrentWindowRect().top + 200;
	LONG xfinish = script->GetCurrentWindowRect().left + 100;

	script->AddAction(mouse->MoveCursorTo(xfinish,yfinish));
	script->AddAction(mouse->LeftButtonClick());
	srand(time(0));
	int random =  rand() % Texts::texts.size();
	script->AddAction(keyboard->PrintText(Texts::texts[random]));
	script->AddAction(SleepAction(1000));
	script->AddAction(keyboard->PressCtrlS());
	script->AddAction(SleepAction(100));
	script->AddAction(mouse->HideWindow(window));
	//script->AddAction(mouse->CloseWindow(window));

	return script;
}
