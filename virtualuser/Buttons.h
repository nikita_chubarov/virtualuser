#pragma once
#include <vector>
#include <queue>
#include <string>
#include <Windows.h>

struct ButtonParams
{
	std::queue<HWND> clickBtns;

	HWND window = 0;
	static std::vector<std::string> buttons;
	static std::vector<std::string> agreements;
	static std::vector<std::string> exclude;
	static std::vector<std::string> pathreq;
};

