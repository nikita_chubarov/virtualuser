#pragma once

#include <queue>
#include <string>
#include <map>
#include <Windows.h>
#include <winnt.h>
#include <thread>
#include "Action.h"

/**
*
*
*
*
*
*
*
*/
class Script
{
public:
	Script();
	virtual Script* Init(HWND window = 0) { return nullptr; }

	void AddAction(Action* action);
	void AddAction(Script* script);
	void AddAction(std::deque<Action*>* script);

	void Work();
	void Stop();

	bool IsDone() const;
	void SetState(bool);

	Action* SleepAction(int milliseconds);
	void SetWindowRect(HWND windowHandle);
	RECT GetCurrentWindowRect() const;

	void SetWindow(HWND);
	HWND GetCurrentWindow() const;

	bool IsWindowOpen();

	DWORD GetPid() const;
	void SetPid(DWORD pid);

protected:
	std::deque<Action*> actions;

	//TODO : ������� ��������� � ����� �����
	HWND windowHandle;
	HWND desktopHandle;

	RECT windowRect;
	RECT desktopRect;

	int _pid;

	bool _state;
	bool isDone;

	class ActionSleep : public Action
	{
	public:
		ActionSleep(int milliseconds) : Action(), _milliseconds(milliseconds) {}
		void Do() override;

	private:
		int _milliseconds;
	};
};

