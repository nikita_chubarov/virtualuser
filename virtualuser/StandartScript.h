#pragma once
#include "Script.h"

class ScriptStandart : public Script
{
public:
	ScriptStandart(std::string name): _name(name) {};
	Script* Init(HWND wnd) override;

private:
	std::string _name;
};

