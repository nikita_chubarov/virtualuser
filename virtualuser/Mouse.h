#pragma once

#include <Windows.h>
#include <math.h>

#include "Action.h"
#include "Script.h"

/**
 *
 *
 *
 */
struct Vector2d
{
	double x;
	double y;
};

/**
 *
 *
 *
 *
 */
class Mouse
{
public:
	Action* LeftButtonClick();
	Action* LeftButtonDoubleClick();
	Action* RightButtonClick();
	Action* Scroll();
	Action* MoveCursorTo(LONG x, LONG y);

	Script* HideAllWindows();
	Script* CloseWindow(HWND);
	Script* HideWindow(HWND);


	static Mouse* GetMouse() 
	{
		if (pInstanse) 
		{
			return pInstanse;
		}
		return new Mouse();
	};

private:	
	Mouse();
	Mouse(const Mouse&) = delete;

	static Mouse* pInstanse;

	class ActionLeftButtonClick : public Action
	{
	public:
		ActionLeftButtonClick() : Action() {}
		void Do() override;
	};

	class ActionRightButtonClick : public Action
	{
	public:
		ActionRightButtonClick() : Action() {}
		void Do() override;
	};

	class ActionLeftButtonDoubleClick : public Action
	{
	public:
		ActionLeftButtonDoubleClick() : Action() {}
		void Do() override;
	};

	class ActionScroll : public Action
	{
	public:
		ActionScroll() : Action() {}
		void Do() override;
	};

	class ActionMoveCursorTo : public Action
	{
	public:
		ActionMoveCursorTo(LONG x, LONG y) : Action(), _x(x), _y(y) {}
		void Do() override;

	private:
		LONG _x, _y;
	};
};
