#include "StandartScript.h"
#include <iostream>
#include "Keyboard.h"
#include "Mouse.h"

Script* ScriptStandart::Init(HWND window)
{
	Script* script = new Script();

	script->SetWindowRect(window);

	RECT rect = script->GetCurrentWindowRect();

	Keyboard* keyboard = Keyboard::GetKeyboard();
	Mouse* mouse = Mouse::GetMouse();

	if (_name == "word") 
	{
		script->AddAction(mouse->HideAllWindows());
		script->AddAction(mouse->MoveCursorTo(rect.left + 20, rect.top + 20));
		script->AddAction(mouse->LeftButtonDoubleClick());
		script->AddAction(SleepAction(1000));

		_name = "";
	}
	else if (_name == "notepad")
	{
		LONG xfinish = rect.right / 2;
		LONG yfinish = rect.bottom / 2;
		
		script->AddAction(mouse->HideAllWindows());

		script->AddAction(mouse->MoveCursorTo(xfinish, yfinish));

		script->AddAction(mouse->RightButtonClick());

		script->AddAction(SleepAction(100));


		for (int i = 0; i < 3; ++i)
		{
			script->AddAction(keyboard->PressKey(VK_UP));
			script->AddAction(SleepAction(100));
		}

		script->AddAction(keyboard->PressKey(VK_RIGHT));
		script->AddAction(SleepAction(100));

		for (int i = 0; i < 3; ++i)
		{
			script->AddAction(keyboard->PressKey(VK_UP));
			script->AddAction(SleepAction(100));
		}

		script->AddAction(keyboard->PressEnter());
		script->AddAction(SleepAction(1000));
		script->AddAction(keyboard->PrintText("��������")); 
		script->AddAction(SleepAction(500));
		script->AddAction(keyboard->PressEnter());
		script->AddAction(keyboard->PressEnter());

		script->AddAction(SleepAction(1000));
		//script->AddAction(keyboard->PressEnter());

		_name = "";
	}
	else if (_name == "doc" || _name == "docx" || _name == "DOC" || _name == "DOCX")
	{
		script->AddAction(mouse->HideAllWindows());
		script->AddAction(keyboard->PressWinR());
		script->AddAction(keyboard->PrintText("calc"));
		script->AddAction(SleepAction(100));
		script->AddAction(keyboard->PressEnter());
		script->AddAction(SleepAction(1000));

		script->AddAction(keyboard->PressKey(VK_NUMPAD3));
		script->AddAction(SleepAction(1000));
		script->AddAction(keyboard->PressKey(VK_ADD));
		script->AddAction(SleepAction(1000));
		script->AddAction(keyboard->PressKey(VK_NUMPAD3));
		script->AddAction(SleepAction(1000));
		script->AddAction(keyboard->PressKey(VK_MULTIPLY));
		script->AddAction(SleepAction(1000));
		script->AddAction(keyboard->PressKey(VK_NUMPAD3));
		script->AddAction(SleepAction(1000));
		script->AddAction(keyboard->PressKey(VK_NUMPAD3));
		script->AddAction(SleepAction(100));
		script->AddAction(keyboard->PressEnter());
		script->AddAction(SleepAction(1000));
		script->AddAction(mouse->CloseWindow(window));

		_name = "";
	}
	else if (_name == "")
	{
		script->SetWindowRect(window);
		
		LONG x = rect.left + rand() % rect.right;
		LONG y = rect.top + rand() % rect.bottom;

		script->AddAction(mouse->MoveCursorTo(x, y));
	}
	return script;
}
