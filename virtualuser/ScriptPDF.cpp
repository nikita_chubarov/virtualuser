#include "ScriptPDF.h"
#include "Keyboard.h"
#include "Mouse.h"
#include <iostream>

ScriptPDF::ScriptPDF()
{
}

int ScriptPDF::GetPagesCount(HWND window)
{
	struct Pages { int count; };
	Pages pages;

	Keyboard* keyboard = Keyboard::GetKeyboard();
	keyboard->PressEnd()->Do();
	Sleep(10);

	HWND flipContainer = FindWindowExA(window, NULL, "AVL_AVView", "AVFlipContainerView");
	HWND mainView = FindWindowExA(flipContainer, NULL, "AVL_AVView", "AVDocumentMainView");
	HWND topBar = FindWindowExA(mainView, NULL, "AVL_AVView", "AVTopBarView");

	WNDENUMPROC proc = [](HWND hwnd, LPARAM lParam)->BOOL
	{
		char tmp[8];

		SendMessageA(hwnd, WM_GETTEXT, 8, (LPARAM)tmp);

		std::cout << "tmp: " << tmp << std::endl;

		if (!(strstr(tmp, "%") || strstr(tmp, "AV")))
		{
			int i = atoi(tmp);
			Pages* k = (Pages*)(lParam);
			k->count = i;
			std::cout << "Size pages lparam: " << k->count << std::endl;
			return FALSE;
		}
		return TRUE;
	};

	EnumChildWindows(topBar, proc, (LPARAM)&pages);

	keyboard->PressHome()->Do();

	std::cout << "Size pages: " << pages.count << std::endl;
	return pages.count;
}

Script* ScriptPDF::Init(HWND window)
{
	Script* script = new Script();

	Keyboard* keyboard = Keyboard::GetKeyboard();
	Mouse* mouse = Mouse::GetMouse();

	script->AddAction(keyboard->PressF5());
		
	int countPages = GetPagesCount(window);

	std::cout << "Size pages: " << countPages << std::endl;
	
	script->AddAction(keyboard->PressKey(VK_CONTROL, VK_OEM_MINUS));
	script->AddAction(keyboard->PressKey(VK_CONTROL, VK_OEM_MINUS));
	
	for (int i = 0; i < countPages; ++i) 
	{
		script->AddAction(keyboard->PressKey(VK_RIGHT));
		script->AddAction(SleepAction(1000));
	}

	script->AddAction(mouse->HideWindow(window));
	return script;
}
