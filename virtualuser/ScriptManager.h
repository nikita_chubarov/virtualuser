#pragma once

#include <queue>
#include <string>
#include <map>

#include "StandartScript.h"
#include "ScriptWord.h"
#include "ScriptPDF.h"
#include "ScriptNotepad.h"

#include "Subscriber.h"
#include "EventNewWindow.h"






class ScriptManager: public Subscriber
{
public:
	ScriptManager(std::string type);

	void Start();
	Script* AddScript(std::string name, HWND window = 0 , DWORD pid = 0);
	Script* AddScript(Script* script);
	void ClearScriptsQueue();
	void Notificate(Event* event) override;

private:
	void StopCurrentScript();
	bool state;
	std::map<std::string, Script*> scriptsDictionary;
	std::map<DWORD, Script*> pids;
	std::deque<Script*> scripts;
};

