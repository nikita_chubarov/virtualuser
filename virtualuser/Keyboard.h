#pragma once
#include <Windows.h>
#include <queue>

#include "Script.h"


class Keyboard 
{
public:
	Action* PrintChar(char key);
	Script* PrintText(const char* text);

	Action* PressKey(BYTE keyFirst, BYTE keySecond = 0, BYTE keyThird = 0);

	Action* PressWinR();
	Action* PressCtrlS();
	Action* HideAllWindow();
	Action* PressF5();
	Action* PressEnter();
	Action* PressEnd();
	Action* PressHome();


	static Keyboard* GetKeyboard() 
	{
		if (pInstanse) 
		{
			return pInstanse;
		}
		return new Keyboard();
	}

private:
	static Keyboard* pInstanse;

	Keyboard();
	Keyboard(const Keyboard&) = delete;

	class ActionPrintChar : public Action
	{
	public:
		ActionPrintChar(char key) : Action() , _key(key) {}
		void Do() override;

	private:
		char _key;
	};

	class ActionPressKey : public Action
	{
	public:
		ActionPressKey(BYTE keyFirst, BYTE keySecond = 0, BYTE keyThird = 0) : Action() , _keyFirst(keyFirst), _keySecond(keySecond), _keyThird(keyThird) {}
		void Do() override;

	private:
		BYTE _keyFirst = 0;
		BYTE _keySecond = 0;
		BYTE _keyThird = 0;
	
	};
};

