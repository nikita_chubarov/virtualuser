#pragma once
#include "Event.h"
class Subscriber
{
public:
	Subscriber();
	virtual void Notificate(Event* = nullptr) = 0;
private:

};

