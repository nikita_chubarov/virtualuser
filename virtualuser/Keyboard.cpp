#include "Keyboard.h"

Keyboard* Keyboard::pInstanse = nullptr;

Keyboard::Keyboard()
{
}

Action* Keyboard::PrintChar(char key)
{
	ActionPrintChar* action = new ActionPrintChar(key);
	return action;
}

Script* Keyboard::PrintText(const char* text)
{
	Script* script = new Script();
	unsigned int milliseconds;
	for (size_t i = 0; i < strlen(text); i++)
	{
		milliseconds = 50 + rand() % 200;
		script->AddAction(PrintChar(text[i]));
		script->AddAction(script->SleepAction(milliseconds));
	}
	return script;
}

Action* Keyboard::PressKey(BYTE keyFirst, BYTE keySecond, BYTE keyThird)
{
	ActionPressKey* action = new ActionPressKey(keyFirst, keySecond, keyThird);
	return action;
}

Action* Keyboard::PressWinR()
{
	ActionPressKey* action = new ActionPressKey(VK_LWIN, 0x52);
	return action;
}

Action* Keyboard::PressCtrlS()
{
	ActionPressKey* action = new ActionPressKey(VK_LCONTROL, 0x53);
	return action;
}

Action* Keyboard::HideAllWindow()
{
	ActionPressKey* action = new ActionPressKey(VK_LWIN, 0x4D); //WIN+M
	return action;
}

Action* Keyboard::PressF5()
{
	ActionPressKey* action = new ActionPressKey(VK_F5);
	return action;
}

Action* Keyboard::PressEnter()
{
	ActionPressKey* action = new ActionPressKey(VK_RETURN);
	return action;
}

Action* Keyboard::PressEnd()
{
	ActionPressKey* action = new ActionPressKey(VK_END);
	return action;
}

Action* Keyboard::PressHome()
{
	ActionPressKey* action = new ActionPressKey(VK_HOME);
	return action;
}

void Keyboard::ActionPrintChar::Do()
{
	wchar_t wc;
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, &_key, 1, &wc, 1);
	INPUT pInput;
	pInput.type = INPUT_KEYBOARD;
	pInput.ki.wVk = 0;
	pInput.ki.time = 10;
	pInput.ki.wScan = wc;
	pInput.ki.dwFlags = KEYEVENTF_UNICODE;
	SendInput(1, &pInput, sizeof(pInput));
	pInput.ki.dwFlags = KEYEVENTF_UNICODE | KEYEVENTF_KEYUP;
	SendInput(1, &pInput, sizeof(pInput));
}

void Keyboard::ActionPressKey::Do()
{
	if (_keyThird) 
	{
		keybd_event(_keyFirst, 0, 0, 0);
		keybd_event(_keySecond, 0, 0, 0);
		keybd_event(_keyThird, 0, 0, 0);
		Sleep(5);
		keybd_event(_keyFirst, 0, KEYEVENTF_KEYUP, 0);
		keybd_event(_keySecond, 0, KEYEVENTF_KEYUP, 0);
		keybd_event(_keyThird, 0, KEYEVENTF_KEYUP, 0);
		Sleep(100);
	}
	else if (_keySecond) 
	{
		keybd_event(_keyFirst, 0, 0, 0);
		keybd_event(_keySecond, 0, 0, 0);
		Sleep(5);
		keybd_event(_keyFirst, 0, KEYEVENTF_KEYUP, 0);
		keybd_event(_keySecond, 0, KEYEVENTF_KEYUP, 0);
		Sleep(100);
	}
	else 
	{
		keybd_event(_keyFirst, 0, 0, 0);
		Sleep(5);
		keybd_event(_keyFirst, 0, KEYEVENTF_KEYUP, 0);
		Sleep(100);
	}
}
