#pragma once

#include <thread>
#include <Windows.h>
#include <vector>
#include <set>

#include "ScriptManager.h"
#include "EventNewWindow.h"
#include "EventExit.h"

class EventsChecker
{
public:
	EventsChecker();

	void CheckForegraundWindow();

	void NotificateAll(Event* event);
	void Subscribe(Subscriber* executer);

	void StartCheckEvents();
	void CheckExit();

private:
	std::vector<Subscriber*> subscribers;

	std::set<const char*> ignoredWindowClasses = {"Progman", "WorkerW", "Shell_TrayWnd"};
	std::set<const char*> ignoredWindowHeaders = {"Выполнить"};

	HWND currentForegraundWindow;
	HWND desktopHandler;
};

