#include "Mouse.h"
#include <iostream>
Mouse* Mouse::pInstanse = nullptr;

Mouse::Mouse()
{}

Action* Mouse::LeftButtonClick()
{
	ActionLeftButtonClick* action = new ActionLeftButtonClick();
	return action;
}

Action* Mouse::LeftButtonDoubleClick()
{
	ActionLeftButtonDoubleClick* action = new ActionLeftButtonDoubleClick();
	return action;
}

Action* Mouse::RightButtonClick()
{
	ActionRightButtonClick* action = new ActionRightButtonClick();
	return action;
}

Action* Mouse::Scroll()
{
	ActionScroll* action = new ActionScroll();
	return action;
}

Action* Mouse::MoveCursorTo(LONG x, LONG y)
{
	ActionMoveCursorTo* action = new ActionMoveCursorTo(x, y);
	return action;
}

Script* Mouse::HideAllWindows()
{
	Script* script = new Script();

	HWND desktop = GetDesktopWindow();
	RECT desktopRect;

	GetWindowRect(desktop, &desktopRect);

	LONG xfinish = desktopRect.right + 10;
	LONG yfinish = desktopRect.bottom + 10;

	script->AddAction(MoveCursorTo(xfinish, yfinish));
	script->AddAction(LeftButtonClick());

	return script;
}

Script* Mouse::CloseWindow(HWND window)
{
	Script* script = new Script();
	script->SetWindowRect(window);

	TITLEBARINFOEX titleBarInfo;
	titleBarInfo.cbSize = sizeof(TITLEBARINFOEX);

	SendMessage(window, WM_GETTITLEBARINFOEX, 0, LPARAM(&titleBarInfo));

	RECT closeRect = titleBarInfo.rgrect[5];

	LONG xfinish = closeRect.right-3;
	LONG yfinish = closeRect.top+3;
	
	script->AddAction(MoveCursorTo(xfinish, yfinish));
	script->AddAction(script->SleepAction(300));
	script->AddAction(LeftButtonClick());
	
	return script;
}

Script* Mouse::HideWindow(HWND window)
{
	Script* script = new Script();
	script->SetWindowRect(window);

	TITLEBARINFOEX titleBarInfo;
	titleBarInfo.cbSize = sizeof(TITLEBARINFOEX);

	SendMessage(window, WM_GETTITLEBARINFOEX, 0, LPARAM(&titleBarInfo));

	RECT closeRect = titleBarInfo.rgrect[2];

	LONG xfinish = closeRect.right - 3;
	LONG yfinish = closeRect.top + 3;

	script->AddAction(MoveCursorTo(xfinish, yfinish));
	script->AddAction(script->SleepAction(200));
	script->AddAction(LeftButtonClick());

	return script;
}


void Mouse::ActionLeftButtonClick::Do()
{
	mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
	Sleep(5);
	mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
}


void Mouse::ActionLeftButtonDoubleClick::Do()
{
	mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
	Sleep(5);
	mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
	mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
	Sleep(5);
	mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
}


void Mouse::ActionRightButtonClick::Do()
{
	mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
	Sleep(5);
	mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
}

void Mouse::ActionScroll::Do()
{
	//TODO add mouse scroll action
}

void Mouse::ActionMoveCursorTo::Do()
{
	POINT currentPosition;
	GetCursorPos(&currentPosition);

	LONG xstart = currentPosition.x;
	LONG ystart = currentPosition.y;

	Vector2d a = { xstart, ystart };
	Vector2d b = { _x , _y };
	Vector2d dist;

	dist.x = b.x - a.x;
	dist.y = b.y - a.y;

	const float step = 0.02f;

	for (float pct = 0.0f; pct < 1.f + step / 2; pct += step)
	{
		Vector2d current;
		current.x = a.x + (pct * dist.x) + 0;
		current.y = a.y + (pow(pct, 4) * dist.y) + 0;

		SetCursorPos(current.x, current.y);

		Sleep(10);
	}
}

